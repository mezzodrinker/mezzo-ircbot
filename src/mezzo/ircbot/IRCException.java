package mezzo.ircbot;

/**
 * <code>IRCException</code>
 * 
 * @author mezzodrinker
 * @since
 */
@SuppressWarnings("serial")
public class IRCException extends RuntimeException {
    public final int    code;
    public final String message;

    public IRCException(int code, String message) {
        this.code = code;
        this.message = message;
    }

    @Override
    public String getMessage() {
        return code + ": " + message;
    }
}
