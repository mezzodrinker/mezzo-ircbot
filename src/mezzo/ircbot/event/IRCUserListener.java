package mezzo.ircbot.event;

import java.util.EventListener;

import org.schwering.irc.lib.IRCUser;

/**
 * <code>IRCUserListener</code>
 * 
 * @author mezzodrinker
 * @since
 */
public interface IRCUserListener extends IRCListener, EventListener {
    public void onJoin(String channel, IRCUser user);

    public void onPart(String channel, IRCUser user, String message);

    public void onQuit(IRCUser user, String message);

    public void onInvite(String channel, IRCUser user, String target);

    public void onNick(IRCUser user, String nick);

    public void onMode(IRCUser user, String source, String mode);
}
