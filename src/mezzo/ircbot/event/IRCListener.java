package mezzo.ircbot.event;

import java.util.EventListener;

/**
 * <code>IRCListener</code>
 * 
 * @author mezzodrinker
 * @since
 */
public interface IRCListener extends EventListener {
    public void onConnected();

    public void onDisconnected();

    public void onRegistered();

    public void onTick();

    public default void unknown(String prefix, String command, String middle, String trailing) {}
}
