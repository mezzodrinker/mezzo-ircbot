package mezzo.ircbot.event;

import java.util.EventListener;

import org.schwering.irc.lib.IRCUser;

/**
 * <code>IRCMessageListener</code>
 * 
 * @author mezzodrinker
 * @since
 */
public interface IRCMessageListener extends IRCListener, EventListener {
    public void onError(String message);

    public void onError(int code, String message);

    public void onNotice(String target, IRCUser source, String message);

    public void onMessage(String target, IRCUser source, String message);

    public void onAction(String channel, IRCUser source, String message);

    public void onReply(int code, String value, String message);

    public void onPing(String ping);
}
