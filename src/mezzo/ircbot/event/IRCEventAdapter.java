package mezzo.ircbot.event;

import java.util.ArrayList;
import java.util.List;

import mezzo.util.StringUtil;
import mezzo.util.logging.Logger;

import org.schwering.irc.lib.IRCEventListener;
import org.schwering.irc.lib.IRCModeParser;
import org.schwering.irc.lib.IRCUser;

/**
 * <code>IRCEventAdapter</code>
 * 
 * @author mezzodrinker
 * @since
 */
public class IRCEventAdapter implements IRCEventListener {
    private List<IRCListener>        listeners        = new ArrayList<>();
    private List<IRCMessageListener> messageListeners = new ArrayList<>();
    private List<IRCChannelListener> channelListeners = new ArrayList<>();
    private List<IRCUserListener>    userListeners    = new ArrayList<>();
    private List<IRCEventListener>   eventListeners   = new ArrayList<>();

    public final Logger              logger;

    public IRCEventAdapter(Logger logger) {
        this.logger = logger;
    }

    public void clear() {
        listeners.clear();
        messageListeners.clear();
        channelListeners.clear();
        userListeners.clear();
        eventListeners.clear();
    }

    public void addListener(IRCListener listener) {
        if (!listeners.contains(listener)) {
            listeners.add(listener);
        }
    }

    public void addMessageListener(IRCMessageListener listener) {
        if (!messageListeners.contains(listener)) {
            messageListeners.add(listener);
        }
        addListener(listener);
    }

    public void addChannelListener(IRCChannelListener listener) {
        if (!channelListeners.contains(listener)) {
            channelListeners.add(listener);
        }
        addListener(listener);
    }

    public void addUserListener(IRCUserListener listener) {
        if (!userListeners.contains(listener)) {
            userListeners.add(listener);
        }
        addListener(listener);
    }

    public void addEventListener(IRCEventListener listener) {
        if (!eventListeners.contains(listener)) {
            eventListeners.add(listener);
        }
    }

    public void removeListener(Object listener) {
        if (listener instanceof IRCListener) {
            listeners.remove(listener);

            if (listener instanceof IRCMessageListener) {
                messageListeners.remove(listener);
            }
            if (listener instanceof IRCChannelListener) {
                channelListeners.remove(listener);
            }
            if (listener instanceof IRCUserListener) {
                userListeners.remove(listener);
            }
        }
        if (listener instanceof IRCEventListener) {
            eventListeners.remove(listener);
        }
    }

    public void onTick() {
        for (IRCListener listener : listeners) {
            try {
                listener.onTick();
            } catch (Throwable t) {
                logger.log(listener + " caused a " + t.getClass().getSimpleName(), t);
            }
        }
    }

    public void onMessage(String target, IRCUser source, String message) {
        for (IRCMessageListener listener : messageListeners) {
            try {
                listener.onMessage(target, source, message);
            } catch (Throwable t) {
                logger.log(listener + " caused a " + t.getClass().getSimpleName(), t);
            }
        }
    }

    public void onAction(String target, IRCUser source, String message) {
        for (IRCMessageListener listener : messageListeners) {
            try {
                listener.onAction(target, source, message);
            } catch (Throwable t) {
                logger.log(listener + " caused a " + t.getClass().getSimpleName(), t);
            }
        }
    }

    public void onConnected() {
        for (IRCListener listener : listeners) {
            try {
                listener.onConnected();
            } catch (Throwable t) {
                logger.log(listener + " caused a " + t.getClass().getSimpleName(), t);
            }
        }
    }

    @Override
    public void onRegistered() {
        for (IRCListener listener : listeners) {
            try {
                listener.onRegistered();
            } catch (Throwable t) {
                logger.log(listener + " caused a " + t.getClass().getSimpleName(), t);
            }
        }

        for (IRCEventListener listener : eventListeners) {
            try {
                listener.onRegistered();
            } catch (Throwable t) {
                logger.log(listener + " caused a " + t.getClass().getSimpleName(), t);
            }
        }
    }

    @Override
    public void onDisconnected() {
        for (IRCListener listener : listeners) {
            try {
                listener.onDisconnected();
            } catch (Throwable t) {
                logger.log(listener + " caused a " + t.getClass().getSimpleName(), t);
            }
        }

        for (IRCEventListener listener : eventListeners) {
            try {
                listener.onDisconnected();
            } catch (Throwable t) {
                logger.log(listener + " caused a " + t.getClass().getSimpleName(), t);
            }
        }
    }

    @Override
    public void onError(String msg) {
        for (IRCMessageListener listener : messageListeners) {
            try {
                listener.onError(msg);
            } catch (Throwable t) {
                logger.log(listener + " caused a " + t.getClass().getSimpleName(), t);
            }
        }

        for (IRCEventListener listener : eventListeners) {
            try {
                listener.onError(msg);
            } catch (Throwable t) {
                logger.log(listener + " caused a " + t.getClass().getSimpleName(), t);
            }
        }
    }

    @Override
    public void onError(int num, String msg) {
        for (IRCMessageListener listener : messageListeners) {
            try {
                listener.onError(num, msg);
            } catch (Throwable t) {
                logger.log(listener + " caused a " + t.getClass().getSimpleName(), t);
            }
        }

        for (IRCEventListener listener : eventListeners) {
            try {
                listener.onError(num, msg);
            } catch (Throwable t) {
                logger.log(listener + " caused a " + t.getClass().getSimpleName(), t);
            }
        }
    }

    @Override
    public void onInvite(String chan, IRCUser user, String passiveNick) {
        for (IRCUserListener listener : userListeners) {
            try {
                listener.onInvite(chan, user, passiveNick);
            } catch (Throwable t) {
                logger.log(listener + " caused a " + t.getClass().getSimpleName(), t);
            }
        }

        for (IRCEventListener listener : eventListeners) {
            try {
                listener.onInvite(chan, user, passiveNick);
            } catch (Throwable t) {
                logger.log(listener + " caused a " + t.getClass().getSimpleName(), t);
            }
        }
    }

    @Override
    public void onJoin(String chan, IRCUser user) {
        for (IRCUserListener listener : userListeners) {
            try {
                listener.onJoin(chan, user);
            } catch (Throwable t) {
                logger.log(listener + " caused a " + t.getClass().getSimpleName(), t);
            }
        }

        for (IRCEventListener listener : eventListeners) {
            try {
                listener.onJoin(chan, user);
            } catch (Throwable t) {
                logger.log(listener + " caused a " + t.getClass().getSimpleName(), t);
            }
        }
    }

    @Override
    public void onKick(String chan, IRCUser user, String passiveNick, String msg) {
        for (IRCChannelListener listener : channelListeners) {
            try {
                listener.onKick(chan, user, passiveNick, msg);
            } catch (Throwable t) {
                logger.log(listener + " caused a " + t.getClass().getSimpleName(), t);
            }
        }

        for (IRCEventListener listener : eventListeners) {
            try {
                listener.onKick(chan, user, passiveNick, msg);
            } catch (Throwable t) {
                logger.log(listener + " caused a " + t.getClass().getSimpleName(), t);
            }
        }
    }

    @Override
    public void onMode(String chan, IRCUser user, IRCModeParser modeParser) {
        for (IRCChannelListener listener : channelListeners) {
            try {
                listener.onMode(chan, user, modeParser);
            } catch (Throwable t) {
                logger.log(listener + " caused a " + t.getClass().getSimpleName(), t);
            }
        }

        for (IRCEventListener listener : eventListeners) {
            try {
                listener.onMode(chan, user, modeParser);
            } catch (Throwable t) {
                logger.log(listener + " caused a " + t.getClass().getSimpleName(), t);
            }
        }
    }

    @Override
    public void onMode(IRCUser user, String passiveNick, String mode) {
        for (IRCUserListener listener : userListeners) {
            try {
                listener.onMode(user, passiveNick, mode);
            } catch (Throwable t) {
                logger.log(listener + " caused a " + t.getClass().getSimpleName(), t);
            }
        }

        for (IRCEventListener listener : eventListeners) {
            try {
                listener.onMode(user, passiveNick, mode);
            } catch (Throwable t) {
                logger.log(listener + " caused a " + t.getClass().getSimpleName(), t);
            }
        }
    }

    @Override
    public void onNick(IRCUser user, String newNick) {
        for (IRCUserListener listener : userListeners) {
            try {
                listener.onNick(user, newNick);
            } catch (Throwable t) {
                logger.log(listener + " caused a " + t.getClass().getSimpleName(), t);
            }
        }

        for (IRCEventListener listener : eventListeners) {
            try {
                listener.onNick(user, newNick);
            } catch (Throwable t) {
                logger.log(listener + " caused a " + t.getClass().getSimpleName(), t);
            }
        }
    }

    @Override
    public void onNotice(String target, IRCUser user, String msg) {
        for (IRCMessageListener listener : messageListeners) {
            try {
                listener.onNotice(target, user, msg);
            } catch (Throwable t) {
                logger.log(listener + " caused a " + t.getClass().getSimpleName(), t);
            }
        }

        for (IRCEventListener listener : eventListeners) {
            try {
                listener.onNotice(target, user, msg);
            } catch (Throwable t) {
                logger.log(listener + " caused a " + t.getClass().getSimpleName(), t);
            }
        }
    }

    @Override
    public void onPart(String chan, IRCUser user, String msg) {
        for (IRCUserListener listener : userListeners) {
            try {
                listener.onPart(chan, user, msg);
            } catch (Throwable t) {
                logger.log(listener + " caused a " + t.getClass().getSimpleName(), t);
            }
        }

        for (IRCEventListener listener : eventListeners) {
            try {
                listener.onPart(chan, user, msg);
            } catch (Throwable t) {
                logger.log(listener + " caused a " + t.getClass().getSimpleName(), t);
            }
        }
    }

    @Override
    public void onPing(String ping) {
        for (IRCMessageListener listener : messageListeners) {
            try {
                listener.onPing(ping);
            } catch (Throwable t) {
                logger.log(listener + " caused a " + t.getClass().getSimpleName(), t);
            }
        }

        for (IRCEventListener listener : eventListeners) {
            try {
                listener.onPing(ping);
            } catch (Throwable t) {
                logger.log(listener + " caused a " + t.getClass().getSimpleName(), t);
            }
        }
    }

    @Override
    public void onPrivmsg(String target, IRCUser user, String message) {
        if (message.startsWith("\u0001")) {
            // CTCP command
            message = message.substring(1, message.length() - 1);
            if (StringUtil.startsWithIgnoreCase(message, "ACTION ")) {
                // action
                String action = message.substring("ACTION ".length());
                onAction(target, user, action);
            }
        } else {
            onMessage(target, user, message);
        }

        for (IRCEventListener listener : eventListeners) {
            try {
                listener.onPrivmsg(target, user, message);
            } catch (Throwable t) {
                logger.log(listener + " caused a " + t.getClass().getSimpleName(), t);
            }
        }
    }

    @Override
    public void onQuit(IRCUser user, String msg) {
        for (IRCUserListener listener : userListeners) {
            try {
                listener.onQuit(user, msg);
            } catch (Throwable t) {
                logger.log(listener + " caused a " + t.getClass().getSimpleName(), t);
            }
        }

        for (IRCEventListener listener : eventListeners) {
            try {
                listener.onQuit(user, msg);
            } catch (Throwable t) {
                logger.log(listener + " caused a " + t.getClass().getSimpleName(), t);
            }
        }
    }

    @Override
    public void onReply(int num, String value, String msg) {
        for (IRCMessageListener listener : messageListeners) {
            try {
                listener.onReply(num, value, msg);
            } catch (Throwable t) {
                logger.log(listener + " caused a " + t.getClass().getSimpleName(), t);
            }
        }

        for (IRCEventListener listener : eventListeners) {
            try {
                listener.onReply(num, value, msg);
            } catch (Throwable t) {
                logger.log(listener + " caused a " + t.getClass().getSimpleName(), t);
            }
        }
    }

    @Override
    public void onTopic(String chan, IRCUser user, String topic) {
        for (IRCChannelListener listener : channelListeners) {
            try {
                listener.onTopic(chan, user, topic);
            } catch (Throwable t) {
                logger.log(listener + " caused a " + t.getClass().getSimpleName(), t);
            }
        }

        for (IRCEventListener listener : eventListeners) {
            try {
                listener.onTopic(chan, user, topic);
            } catch (Throwable t) {
                logger.log(listener + " caused a " + t.getClass().getSimpleName(), t);
            }
        }
    }

    @Override
    public void unknown(String prefix, String command, String middle, String trailing) {
        for (IRCListener listener : listeners) {
            try {
                listener.unknown(prefix, command, middle, trailing);
            } catch (Throwable t) {
                logger.log(listener + " caused a " + t.getClass().getSimpleName(), t);
            }
        }

        for (IRCEventListener listener : eventListeners) {
            try {
                listener.unknown(prefix, command, middle, trailing);
            } catch (Throwable t) {
                logger.log(listener + " caused a " + t.getClass().getSimpleName(), t);
            }
        }
    }
}
