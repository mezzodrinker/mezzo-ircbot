package mezzo.ircbot.event;

import java.util.EventListener;

import org.schwering.irc.lib.IRCModeParser;
import org.schwering.irc.lib.IRCUser;

/**
 * <code>IRCChannelListener</code>
 * 
 * @author mezzodrinker
 * @since
 */
public interface IRCChannelListener extends IRCListener, EventListener {
    public void onKick(String channel, IRCUser user, String source, String message);

    public void onMode(String channel, IRCUser user, IRCModeParser modeParser);

    public void onTopic(String channel, IRCUser user, String topic);
}
