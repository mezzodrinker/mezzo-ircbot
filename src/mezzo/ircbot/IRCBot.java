package mezzo.ircbot;

import java.io.IOException;
import java.nio.charset.Charset;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.Timer;

import mezzo.ircbot.event.IRCEventAdapter;
import mezzo.util.HashTable;
import mezzo.util.logging.Logger;
import mezzo.util.logging.stream.DefaultPrintStreamLogger;
import mezzo.util.time.Time;
import mezzo.util.version.Version;

import org.schwering.irc.lib.IRCConstants;
import org.schwering.irc.lib.IRCEventListener;
import org.schwering.irc.lib.IRCModeParser;
import org.schwering.irc.lib.IRCUser;

/**
 * <code>IRCBot</code>
 * 
 * @author mezzodrinker
 * @since 1.0.0
 */
public class IRCBot implements IRCEventListener {
    private boolean                                 colored           = true;
    private boolean                                 autopong          = true;
    private int                                     timeout           = 15 * 60 * 1000;
    private int[]                                   ports             = null;
    private long                                    cooldown          = 0L;
    private long                                    lastMessageSentAt = 0;
    private String                                  host              = null;
    private String                                  username          = "mezzo-ircbot";
    private String                                  serverPassword    = null;
    private String                                  nick              = "mezzo-ircbot";
    private String                                  realname          = "IRCBot v" + VERSION + " by mezzo";
    private Charset                                 charset           = Charset.forName("UTF-8");
    private IRCConnection                           connection        = null;
    private Map<String, Set<IRCUser>>               users             = new HashMap<>();
    private HashTable<IRCUser, String, IRCUserMode> modes             = new HashTable<>();

    protected String                                mainChannel       = null;
    protected long                                  timeConnected     = 0;

    public final Logger                             logger;
    public final IRCEventAdapter                    events;
    public final Timer                              timer             = new Timer("IRCBot timer", true);

    public static final Version                     VERSION           = Version.valueOf("1.0.0");

    protected IRCBot(Logger logger) {
        this(logger, new IRCEventAdapter(logger));
    }

    protected IRCBot(Logger logger, IRCEventAdapter events) {
        this.logger = logger;
        this.events = events;
        events.addEventListener(this);
    }

    protected IRCBot(Logger logger, String host, int port) {
        this(logger);
        setHost(host);
        setPort(port);
    }

    protected IRCBot(Logger logger, String host, int[] ports) {
        this(logger);
        setHost(host);
        setPorts(ports);
    }

    protected IRCBot(Logger logger, String host, int minPort, int maxPort) {
        this(logger);
        setHost(host);
        setPorts(minPort, maxPort);
    }

    private void checkIfConnected() throws IllegalStateException {
        if (!isConnected()) throw new IllegalStateException("not connected");
    }

    private IRCUserMode getUserMode(String nick, String channel) {
        checkIfConnected();
        IRCUser user = getUser(nick);
        if (user == null) return null;
        connection.doWhois(nick);
        return modes.get(user, channel);
    }

    public void setHost(String host) {
        if (isConnected()) return;
        this.host = host;
    }

    public String getHost() {
        return host;
    }

    public void setPort(int port) {
        if (isConnected()) return;
        setPorts(new int[] {port});
    }

    public void setPorts(int[] ports) {
        if (isConnected()) return;
        this.ports = ports;
    }

    public void setPorts(int min, int max) {
        if (isConnected()) return;
        if (max < min) throw new IllegalArgumentException("max has to be greater than or equal to min");

        int[] ports = new int[max - min + 1];
        for (int i = min; i < max; i++) {
            ports[i - min] = i;
        }
        setPorts(ports);
    }

    public int[] getPorts() {
        return ports;
    }

    public void setServerPassword(String serverPassword) {
        if (isConnected()) return;
        this.serverPassword = serverPassword;
    }

    public String getServerPassword() {
        return serverPassword;
    }

    public void setNick(String nick) {
        if (isConnected()) {
            connection.doNick(nick);
        }
        this.nick = nick;
    }

    public String getNick() {
        return nick;
    }

    public void setUsername(String username) {
        if (isConnected()) return;
        this.username = username;
    }

    public String getUsername() {
        return username;
    }

    public void setRealname(String realname) {
        if (isConnected()) return;
        this.realname = realname;
    }

    public String getRealname() {
        return realname;
    }

    public void setColored(boolean colored) {
        if (isConnected()) {
            connection.setColors(colored);
        }
        this.colored = colored;
    }

    public boolean isColored() {
        return colored;
    }

    public void setCharset(String charset) {
        setCharset(Charset.forName(charset));
    }

    public void setCharset(Charset charset) {
        if (isConnected()) {
            connection.setEncoding(charset.name());
        }
        this.charset = charset;
    }

    public Charset getCharset() {
        return charset;
    }

    public void setAutoPong(boolean autopong) {
        if (isConnected()) {
            connection.setPong(autopong);
        }
        this.autopong = autopong;
    }

    public boolean isAutoPong() {
        return autopong;
    }

    public void setTimeout(int timeout) {
        if (isConnected()) {
            connection.setTimeout(timeout);
        }
        this.timeout = timeout;
    }

    public int getTimeout() {
        return timeout;
    }

    public void setCooldown(long cooldown) {
        this.cooldown = cooldown;
    }

    public long getCooldown() {
        return cooldown;
    }

    public IRCUser getUser(String nick) {
        return getUser(nick, mainChannel);
    }

    public IRCUser getUser(String nick, String channel) {
        for (IRCUser user : getUsers(channel)) {
            if (user.getNick().equals(nick)) return user;
        }
        return null;
    }

    public IRCUser[] getUsers() {
        return getUsers(mainChannel);
    }

    public IRCUser[] getUsers(String channel) {
        Set<IRCUser> users = this.users.get(channel);
        return users == null ? new IRCUser[0] : users.toArray(new IRCUser[0]);
    }

    public boolean isOp(String nick) {
        return isOp(nick, mainChannel);
    }

    public boolean isOp(String nick, String channel) {
        IRCUserMode userMode = getUserMode(nick, channel);
        if (userMode == null) return false;
        return userMode.isOp();
    }

    public boolean hasVoice(String nick) {
        return hasVoice(nick, mainChannel);
    }

    public boolean hasVoice(String nick, String channel) {
        IRCUserMode userMode = getUserMode(nick, channel);
        if (userMode == null) return false;
        return userMode.hasVoice();
    }

    public synchronized void connect() throws IOException {
        if (isConnected()) throw new IllegalStateException("already connected");
        connection = new IRCConnection(host, ports, serverPassword, nick, username, realname);
        connection.setColors(colored);
        connection.setEncoding(charset.name());
        connection.setPong(autopong);
        connection.setTimeout(timeout);
        connection.addIRCEventListener(events);
        connection.connect();
        timeConnected = System.currentTimeMillis();
        events.onConnected();
    }

    public synchronized void reconnect() throws IOException {
        if (isConnected()) throw new IllegalStateException("already connected");
        if (connection == null) throw new IllegalStateException("not connected yet");
        connection.setColors(colored);
        connection.setEncoding(charset.name());
        connection.setPong(autopong);
        connection.setTimeout(timeout);
        connection.addIRCEventListener(events);
        connection.connect();
        timeConnected = System.currentTimeMillis();
        mainChannel = null;
        events.onConnected();
    }

    public synchronized void disconnect() {
        if (!isConnected()) throw new IllegalStateException("not connected");
        connection.doQuit();
        connection.close();
        mainChannel = null;
    }

    public void join(String channel) {
        checkIfConnected();
        connection.doJoin(channel);
        mainChannel = channel;
    }

    public void join(String channel, String key) {
        checkIfConnected();
        connection.doJoin(channel, key);
        mainChannel = channel;
    }

    public void leave(String channel) {
        checkIfConnected();
        connection.doPart(channel);
        if (channel.equals(mainChannel)) {
            mainChannel = null;
        }
    }

    public void leave(String channel, String message) {
        checkIfConnected();
        connection.doPart(channel, message);
        if (channel.equals(mainChannel)) {
            mainChannel = null;
        }
    }

    public void sendMessage(String message) {
        sendMessage(mainChannel, message);
    }

    public void sendMessage(String target, String message) {
        checkIfConnected();
        String[] messages = message.split("(\r\n|\n\r|\r|\n)");
        for (String s : messages) {
            new PrivmsgSendThread(target, s);
        }
    }

    public void sendAction(String message) {
        sendAction(mainChannel, message);
    }

    public void sendAction(String channel, String message) {
        checkIfConnected();
        sendCTCPCommand(channel, "ACTION " + message);
    }

    public void sendCTCPCommand(String target, String command) {
        checkIfConnected();
        new CTCPSendThread(target, command);
    }

    public void away() {
        away("away since " + Time.getCurrentDateTimeAsString() + " " + Time.getTimezoneAsString());
    }

    public void away(String message) {
        checkIfConnected();
        connection.doAway(message);
    }

    public void back() {
        checkIfConnected();
        connection.doAway();
    }

    public void identify(String password) {
        checkIfConnected();
        sendMessage("NickServ", "IDENTIFY " + password);
    }

    public boolean isConnected() {
        return connection != null && connection.isAlive() && connection.isConnected();
    }

    /**
     * Fired when a connection was successfully established.
     * 
     * <p>
     * The default implementation does not do anything.
     * </p>
     */
    public void onConnected() {}

    /**
     * {@inheritDoc}
     * 
     * <p>
     * <strong>NOTE:</strong> if you intend to override this method, include <code>super.onReply(int, String, String)</code> IN ANY CASE or things <em>will</em> go wrong.
     * </p>
     */
    @Override
    public void onReply(int num, String value, String msg) {
        // logger.debug(StringUtil.copyOf(IRCUtil.replyCode(num), 10) + " " + value + " | " + msg);
        if (num == IRCConstants.RPL_NAMREPLY) { // code 353
            String[] values = value.split(" ");
            String[] nicknames = msg.split(" ");
            String channel = values[values.length - 1];
            Set<IRCUser> usersKnown = users.get(channel);
            if (usersKnown == null) {
                usersKnown = new HashSet<>();
            }
            Set<IRCUser> users = new HashSet<>();
            users.addAll(usersKnown);

            for (String nickname : nicknames) {
                boolean isOp = nickname.indexOf('@') >= 0;
                boolean hasVoice = nickname.indexOf('+') >= 0;
                nickname = nickname.replaceAll("[+@]", "");
                IRCUser user = new IRCUser(nickname, null, null);
                IRCUserMode mode = modes.get(user, channel);
                if (mode == null) {
                    mode = IRCUserMode.USER;
                }
                if (isOp) {
                    mode.add(IRCUserMode.CHAR_OP);
                }
                if (hasVoice) {
                    mode.add(IRCUserMode.CHAR_VOICE);
                }
                modes.set(user, channel, mode);
                users.add(user);
            }

            this.users.put(channel, users);

            for (IRCUser user : users) {
                if (user.getHost() == null || user.getUsername() == null) {
                    connection.doWhois(user.getNick());
                }
            }
        } else if (num == IRCConstants.RPL_WHOISCHANNELS) { // code 319
            String[] values = value.split(" ");
            String nickname = values[1];
            IRCUser user = getUser(nickname);
            if (user == null) return;

            String[] channels = msg.split(" ");
            for (String channel : channels) {
                if (channel.isEmpty()) {
                    continue;
                }
                boolean isOp = channel.indexOf('@') >= 0;
                boolean hasVoice = channel.indexOf('+') >= 0;
                channel = channel.replaceAll("[+@]", "");
                IRCUserMode mode = modes.get(user, channel);
                if (mode == null) {
                    mode = IRCUserMode.USER;
                }
                if (isOp) {
                    mode.add(IRCUserMode.CHAR_OP);
                }
                if (hasVoice) {
                    mode.add(IRCUserMode.CHAR_VOICE);
                }
                modes.set(user, channel, mode);
            }
        } else if (num == IRCConstants.RPL_WHOISUSER) { // code 311
            String[] values = value.split(" ");
            String nickname = values[1];
            String username = values[2];
            String host = values[3];
            Map<String, Set<IRCUser>> updates = new HashMap<>();
            for (Entry<String, Set<IRCUser>> entry : users.entrySet()) {
                String channel = entry.getKey();
                Set<IRCUser> users = entry.getValue();

                for (IRCUser user : users) {
                    if (user.getNick().equals(nickname)) {
                        users.remove(user);
                        IRCUserMode userMode = modes.remove(user, channel);
                        IRCUser newUser = new IRCUser(nickname, username, host);
                        users.add(newUser);
                        modes.set(newUser, channel, userMode);
                        break;
                    }
                }

                updates.put(channel, users);
            }
            users.putAll(updates);
        }
    }

    /**
     * {@inheritDoc}
     * 
     * <p>
     * <strong>NOTE:</strong> if you intend to override this method, include <code>super.onJoin(IRCUser, String)</code> IN ANY CASE or things <em>will</em> go wrong.
     * </p>
     */
    @Override
    public void onJoin(String chan, IRCUser source) {
        Set<IRCUser> users = this.users.get(chan);
        if (users == null) {
            users = new HashSet<>();
        }
        users.add(source);
        modes.set(source, chan, IRCUserMode.USER);
        this.users.put(chan, users);
        getUserMode(chan, source.getNick());
    }

    /**
     * {@inheritDoc}
     * 
     * <p>
     * <strong>NOTE:</strong> if you intend to override this method, include <code>super.onKick(String, IRCUser, String, String)</code> IN ANY CASE or things <em>will</em> go wrong.
     * </p>
     */
    @Override
    public void onKick(String chan, IRCUser source, String passiveNick, String msg) {
        Set<IRCUser> users = this.users.get(chan);
        if (users == null) {
            users = new HashSet<>();
        }
        for (IRCUser chanUser : users) {
            if (chanUser.getNick().equals(source.getNick())) {
                users.remove(chanUser);
                break;
            }
        }
        this.users.put(chan, users);
    }

    /**
     * {@inheritDoc}
     * 
     * <p>
     * <strong>NOTE:</strong> if you intend to override this method, include <code>super.onNick(IRCUser, String)</code> IN ANY CASE or things <em>will</em> go wrong.
     * </p>
     */
    @Override
    public void onNick(IRCUser source, String newNick) {
        Map<String, Set<IRCUser>> updates = new HashMap<>();
        for (Entry<String, Set<IRCUser>> entry : users.entrySet()) {
            String channel = entry.getKey();
            Set<IRCUser> users = entry.getValue();
            if (users == null) {
                users = new HashSet<>();
            }

            for (IRCUser user : users) {
                if (user.getNick().equals(source.getNick())) {
                    users.remove(user);
                    users.add(new IRCUser(newNick, source.getUsername(), source.getHost()));
                    break;
                }
            }

            updates.put(channel, users);
        }
        users.putAll(updates);
    }

    /**
     * {@inheritDoc}
     * 
     * <p>
     * <strong>NOTE:</strong> if you intend to override this method, include <code>super.onPart(String, IRCUser, String)</code> IN ANY CASE or things <em>will</em> go wrong.
     * </p>
     */
    @Override
    public void onPart(String chan, IRCUser source, String msg) {
        Set<IRCUser> users = this.users.get(chan);
        if (users == null) {
            users = new HashSet<>();
        }
        for (IRCUser user : users) {
            if (user.getNick().equals(user.getNick())) {
                users.remove(user);
                modes.remove(user, chan);
                break;
            }
        }
        this.users.put(chan, users);
    }

    /**
     * {@inheritDoc}
     * 
     * <p>
     * <strong>NOTE:</strong> if you intend to override this method, include <code>super.onQuit(IRCUser, String)</code> IN ANY CASE or things <em>will</em> go wrong.
     * </p>
     */
    @Override
    public void onQuit(IRCUser source, String msg) {
        Map<String, Set<IRCUser>> updates = new HashMap<>();
        for (Entry<String, Set<IRCUser>> entry : users.entrySet()) {
            String channel = entry.getKey();
            Set<IRCUser> users = entry.getValue();
            if (users == null) {
                users = new HashSet<>();
            }

            for (IRCUser user : users) {
                if (user.getNick().equals(source.getNick())) {
                    users.remove(user);
                    modes.remove(user, channel);
                    break;
                }
            }

            updates.put(channel, users);
        }
        users.putAll(updates);
    }

    /**
     * {@inheritDoc}
     * 
     * <p>
     * <strong>NOTE:</strong> if you intend to override this method, include <code>super.onMode(String, IRCUser, String)</code> IN ANY CASE or things <em>will</em> go wrong.
     * </p>
     */
    @Override
    public void onMode(String channel, IRCUser source, IRCModeParser modeParser) {
        for (int i = 1; i <= modeParser.getCount(); i++) {
            char operator = modeParser.getOperatorAt(i);
            char modeChar = modeParser.getModeAt(i);
            String target = modeParser.getArgAt(i);
            if (target == null || target.isEmpty()) {
                continue;
            }

            IRCUserMode mode = getUserMode(target, channel);
            if (mode == null) {
                mode = IRCUserMode.USER;
            }

            IRCUser user = getUser(target, channel);
            if (user == null) {
                continue;
            }

            if (operator == '+') {
                // add mode
                mode.add(modeChar);
            } else if (operator == '-') {
                // remove mode
                mode.remove(modeChar);
            }
            modes.set(user, channel, mode);
            System.out.println(target + " on " + channel + " is now " + mode);
        }
    }

    /**
     * {@inheritDoc}
     * 
     * <p>
     * The default implementation does not do anything.
     * </p>
     */
    @Override
    public void onPrivmsg(String target, IRCUser source, String message) {}

    /**
     * {@inheritDoc}
     * 
     * <p>
     * The default implementation does not do anything.
     * </p>
     */
    @Override
    public void onError(int num, String msg) {}

    /**
     * {@inheritDoc}
     * 
     * <p>
     * The default implementation does not do anything.
     * </p>
     */
    @Override
    public void onDisconnected() {}

    /**
     * {@inheritDoc}
     * 
     * <p>
     * The default implementation does not do anything.
     * </p>
     */
    @Override
    public void onRegistered() {}

    /**
     * {@inheritDoc}
     * 
     * <p>
     * The default implementation does not do anything.
     * </p>
     */
    @Override
    public void onError(String msg) {}

    /**
     * {@inheritDoc}
     * 
     * <p>
     * The default implementation does not do anything.
     * </p>
     */
    @Override
    public void onInvite(String chan, IRCUser source, String passiveNick) {}

    /**
     * {@inheritDoc}
     * 
     * <p>
     * The default implementation does not do anything.
     * </p>
     */
    @Override
    public void onNotice(String target, IRCUser source, String msg) {}

    /**
     * {@inheritDoc}
     * 
     * <p>
     * The default implementation does not do anything.
     * </p>
     */
    @Override
    public void onMode(IRCUser source, String target, String mode) {}

    /**
     * {@inheritDoc}
     * 
     * <p>
     * The default implementation does not do anything.
     * </p>
     */
    @Override
    public void onPing(String ping) {}

    /**
     * {@inheritDoc}
     * 
     * <p>
     * The default implementation does not do anything.
     * </p>
     */
    @Override
    public void onTopic(String chan, IRCUser source, String topic) {}

    /**
     * {@inheritDoc}
     * 
     * <p>
     * The default implementation does not do anything.
     * </p>
     */
    @Override
    public void unknown(String prefix, String command, String middle, String trailing) {}

    public static void main(String[] args) {
        try {
            IRCBot bot = new IRCBot(new DefaultPrintStreamLogger(System.out));
            bot.setHost("irc.esper.net");
            bot.setPort(5555);
            bot.connect();
            bot.join("#mobtalker");
            bot.sendAction("is testing some stuff");
        } catch (Throwable t) {
            t.printStackTrace();
        }
    }

    private class PrivmsgSendThread extends Thread {
        public final String target;
        public final String message;

        public PrivmsgSendThread(String target, String message) {
            this.target = target;
            this.message = message;

            start();
        }

        @Override
        public void run() {
            while (System.currentTimeMillis() - lastMessageSentAt < cooldown) {
                try {
                    Thread.sleep(100);
                } catch (InterruptedException e) {
                }
            }
            connection.doPrivmsg(target, message);
            lastMessageSentAt = System.currentTimeMillis();
        }
    }

    private class CTCPSendThread extends Thread {
        public final String target;
        public final String command;

        public CTCPSendThread(String target, String command) {
            this.target = target;
            this.command = command;

            start();
        }

        @Override
        public void run() {
            while (System.currentTimeMillis() - lastMessageSentAt < cooldown) {
                try {
                    Thread.sleep(100);
                } catch (InterruptedException e) {
                }
            }
            connection.doPrivmsg(target, "\u0001" + command + "\u0001");
            lastMessageSentAt = System.currentTimeMillis();
        }
    }
}
