package mezzo.ircbot;

import java.io.IOException;

/**
 * <code>MezzoIRCConnection</code>
 * 
 * @author mezzodrinker
 * @since
 */
public class IRCConnection extends org.schwering.irc.lib.IRCConnection {
    public IRCConnection(String host, int[] ports, String pass, String nick, String username, String realname) {
        super(host, ports, pass, nick, username, realname);
    }

    public IRCConnection(String host, int portMin, int portMax, String pass, String nick, String username, String realname) {
        super(host, portMin, portMax, pass, nick, username, realname);
    }

    @Override
    public void connect() throws IOException {
        super.connect();
        while (level < 2) {
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
            }
        }
    }

    @Override
    public boolean isConnected() {
        return level >= 2;
    }
}
