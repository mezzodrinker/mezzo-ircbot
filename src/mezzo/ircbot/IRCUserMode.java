package mezzo.ircbot;

import java.util.Arrays;

/**
 * <code>IRCUserMode</code>
 * 
 * @author mezzodrinker
 * @since
 */
public class IRCUserMode implements Comparable<IRCUserMode> {
    private String                  modes;

    public static final char        CHAR_VOICE   = 'v';
    public static final char        CHAR_OP      = 'o';
    public static final char        PREFIX_VOICE = '+';
    public static final char        PREFIX_OP    = '@';
    public static final IRCUserMode USER         = new IRCUserMode("");
    public static final IRCUserMode VOICE        = new IRCUserMode(CHAR_VOICE);
    public static final IRCUserMode OP           = new IRCUserMode(CHAR_OP);

    public IRCUserMode(char mode) {
        modes = String.valueOf(mode);
    }

    public IRCUserMode(String modes) {
        this.modes = modes;
    }

    public boolean isOp() {
        return modes.indexOf(CHAR_OP) >= 0;
    }

    public boolean hasVoice() {
        return modes.indexOf(CHAR_VOICE) >= 0;
    }

    public boolean isAtMost(IRCUserMode mode) {
        return compareTo(mode) <= 0;
    }

    public boolean isAtLeast(IRCUserMode mode) {
        return compareTo(mode) >= 0;
    }

    public void add(char mode) {
        if (modes.indexOf(mode) < 0) {
            modes += mode;
        }
    }

    public void remove(char mode) {
        char[] chars = modes.toCharArray();
        char[] newMode = new char[chars.length];
        int c = 0;
        for (char character : chars) {
            if (character == mode) {
                continue;
            }
            newMode[c++] = character;
        }
        modes = new String(Arrays.copyOf(newMode, c));
    }

    @Override
    public boolean equals(Object o) {
        if (!(o instanceof IRCUserMode)) return false;
        return compareTo((IRCUserMode) o) == 0;
    }

    @Override
    public String toString() {
        return getClass().getName() + "{modes=" + modes + "}";
    }

    @Override
    public int compareTo(IRCUserMode mode) {
        if (isOp() && mode.isOp()) return 0;
        if (isOp() && !mode.isOp()) return 1;
        if (!isOp() && mode.isOp()) return -1;

        if (hasVoice() && mode.hasVoice()) return 0;
        if (hasVoice() && !mode.hasVoice()) return 1;
        if (!hasVoice() && mode.hasVoice()) return -1;

        return 0;
    }
}
